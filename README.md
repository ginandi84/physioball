# README #

This is a really silly app I wrote, for two reasons

1.  It forces me to sit oh a physio ball for 10 minutes every day, which is good for my back

2.  Wanted to have a go at writing an Android app with Kotlin

### So.. ###

My Kotlin is poor. This is the first time I'm writing Kotlin. 
What I basically did was write Java, but in Kotlin.

I did find the part of nullability particularly annoying, because in Android you have to interface a lot with Java, and there everything is nullable.

Another interesting thing I learned is what happens when you combine if's and nullables.

```
#!kotlin

if (list?.size == 0)
```
Well, in case that list is a null, this actually returns false. Interesting