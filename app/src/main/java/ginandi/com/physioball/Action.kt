package ginandi.com.physioball

import ginandi.com.physioball.ActionNames.DISMISS_ACTION_NAME
import ginandi.com.physioball.ActionNames.DONE_ACTION_NAME
import ginandi.com.physioball.ActionNames.SNOOZE_ACTION_NAME
import ginandi.com.physioball.ActionNames.START_ACTION_NAME

/**
 * Created by gindi on 6/18/17.
 */
enum class Action {
    START {
        override val label: String = "Start"

        override val actionName: String = START_ACTION_NAME

    },
    DONE {
        override val label: String = "Done"
        override val actionName: String = DONE_ACTION_NAME

    },
    DISMISS {
        override val actionName: String = DISMISS_ACTION_NAME
        override val label: String = "Dismiss"

    },
    SNOOZE {
        override val actionName: String = SNOOZE_ACTION_NAME
        override val label: String = "Snooze"
    };

    abstract val actionName: String;
    abstract val label: String;
}