package ginandi.com.physioball

import android.app.job.JobInfo
import android.app.job.JobInfo.Builder
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import org.joda.time.Duration

import kotlin.jvm.javaClass

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        var componentName = ComponentName(this, AlarmJobService::class.java)
        val jobInfo = Builder(0, componentName)
                .setMinimumLatency(0)
                .setPeriodic(Duration.standardHours(1).millis)
                .setPersisted(true)
                .build()

        val jobScheduler: JobScheduler = getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        val scheduleId = jobScheduler.schedule(jobInfo)
        Log.d("ggggg", "Schedule id is " + scheduleId)
    }
}
