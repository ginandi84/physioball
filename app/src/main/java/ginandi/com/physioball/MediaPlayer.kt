package ginandi.com.physioball

import android.content.Context
import android.content.res.AssetFileDescriptor
import android.media.AudioAttributes
import android.media.SoundPool
import java.util.*

/**
 * Created by gindi on 6/23/17.
 */
object LoopingMediaPlayer {
    private val soundPool: SoundPool = SoundPool.Builder()
            .setMaxStreams(5)
            .setAudioAttributes(AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build())
            .build()
    private var streamId = -1
    private var allSounds: Array<String>? = null

    fun startPlaying(c: Context) {
        val load = soundPool.load(getRandomSound(c), 100)
         soundPool.setOnLoadCompleteListener { soundPool, sampleId, status -> streamId = soundPool.play(load, 0.99f, 0.99f, 100, -1, 1f) }
    }

    fun stopPlaying() {
        if (streamId != -1) {
            soundPool.stop(streamId)
        }
    }

    private fun getRandomSound(c: Context): AssetFileDescriptor? {
        if (allSounds == null || allSounds?.size == 0) {
            allSounds = c.assets.list("sounds")
            if (allSounds?.size == 0) return null
        }

        val rand = Random()
        val path = "sounds/%s".format(allSounds?.get(rand.nextInt(allSounds?.size ?: 0)))

        return c.assets.openFd(path)
    }
}