package ginandi.com.physioball

import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.os.CountDownTimer
import android.os.IBinder

/**
 * Created by gindi on 6/24/17.
 */

const val STOP_COMMAND: String = "ginandi.com.physioball.stop"

class CountdownService : Service() {
    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action == STOP_COMMAND) {
            stopForeground(true)
            PhysioCounter.stop()
            return super.onStartCommand(intent, flags, startId)
        }

        val notification = PhysioNotificationManager(this).builderWithActions(Action.DONE).build();

        PhysioCounter.start(this)

        startForeground(ID, notification);

        return super.onStartCommand(intent, flags, startId)
    }
}