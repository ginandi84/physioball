package ginandi.com.physioball

import android.content.Context
import android.os.CountDownTimer

/**
 * Created by gindi on 6/24/17.
 */
object PhysioCounter {

    private var countDownTimer: CountDownTimer? = null

    fun start(c: Context) {
        val notificationManager = PhysioNotificationManager(c)

        countDownTimer = object : CountDownTimer(600000, 1000) {

            override fun onTick(millisUntilFinished: Long) {
                notificationManager.notify(
                        notificationManager.builderWithActions(Action.DONE).setContentTitle("Seconds remaining: " + millisUntilFinished / 1000))
            }

            override fun onFinish() {
                LoopingMediaPlayer.startPlaying(c)
                notificationManager.notify(notificationManager.withDismissWhenTap(notificationManager.builderWithActions(Action.DONE).setContentTitle("Done")))
            }
        }.start()
    }

    fun stop() {
        countDownTimer?.cancel()
    }
}