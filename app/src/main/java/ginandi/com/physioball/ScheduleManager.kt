package ginandi.com.physioball

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import org.joda.time.DateTime

/**
 * Created by gindi on 6/17/17.
 */
const val LAST_TIME_USED_PARAM = "last_time_used_physio"
fun getScheduleManager(context: Context) = ScheduleManager.withContext(context)

object ScheduleManager {
    private var context: Context? = null
    private var sharedPrefs: SharedPreferences? = null

    fun withContext(newContext: Context): ScheduleManager {
        context = newContext
        sharedPrefs = newContext.getSharedPreferences("physioball", MODE_PRIVATE)
        return this
    }

    var lastTimeUsePhysioBall: DateTime
        get() {
            return DateTime(sharedPrefs?.getLong(LAST_TIME_USED_PARAM, 0))
        }
        set(value) {
            sharedPrefs?.edit()?.putLong(LAST_TIME_USED_PARAM, value.millis)?.apply()
        }
}
