package ginandi.com.physioball

import android.app.job.JobParameters
import android.app.job.JobService
import org.joda.time.DateTime

/**
 * Created by gindi on 6/17/17.
 */
class AlarmJobService : JobService() {
    override fun onStopJob(params: JobParameters?): Boolean {
        return true
    }

    override fun onStartJob(params: JobParameters?): Boolean {

        val hourOfDay = DateTime.now().hourOfDay
        if (hourOfDay < 9 || hourOfDay > 22) return false

        val lastTimeUsePhysioBall = getScheduleManager(this).lastTimeUsePhysioBall

        if (lastTimeUsePhysioBall.isBefore(DateTime.now().withTimeAtStartOfDay())) {
            val notificationManager = PhysioNotificationManager(this)
            notificationManager.notify(notificationManager.builderWithActions(ginandi.com.physioball.Action.START, ginandi.com.physioball.Action.SNOOZE, ginandi.com.physioball.Action.DISMISS))
        }

        return false
    }
}