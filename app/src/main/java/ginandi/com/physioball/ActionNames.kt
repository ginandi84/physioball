package ginandi.com.physioball

/**
 * Created by gindi on 6/17/17.
 */
object ActionNames {
    const val START_ACTION_NAME = "ginandi.com.pyhsioball.start"
    const val TAP_ACTION_NAME = "ginandi.com.pyhsioball.tap"
    const val DISMISS_ACTION_NAME = "ginandi.com.pyhsioball.dismiss"
    const val DONE_ACTION_NAME = "ginandi.com.pyhsioball.done"
    const val SNOOZE_ACTION_NAME = "ginandi.com.pyhsioball.snooze"
}