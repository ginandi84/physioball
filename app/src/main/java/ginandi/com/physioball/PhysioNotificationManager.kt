package ginandi.com.physioball

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Icon

/**
 * Created by gindi on 6/17/17.
 */

const val ID = 1780

class PhysioNotificationManager(val context: Context) {

    fun builderWithActions(vararg actions: Action): Notification.Builder {
        val builder = Notification.Builder(context)
                .setContentText("Physio ball")
                .setContentTitle("It's time to sit on it")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(tapIntent)
                .setOngoing(true)

        actions.asIterable().forEach({
            builder.addAction(getAction(it))
        });

        return builder
    }

    fun withDismissWhenTap(builder: Notification.Builder) = builder.setContentIntent(dismissIntent)

    fun notify(builder: Notification.Builder) =
            notificationManager.notify(
                    ID,
                    builder.build())

    fun cancel() = notificationManager.cancel(ID)

    private val notificationManager = (context?.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)


    private fun getAction(action: Action): Notification.Action {
        val actionIntent = Intent(context, NotificationReceiver::class.java)
        actionIntent.action = action.actionName
        return Notification.Action.Builder(Icon.createWithResource(context, android.R.drawable.btn_plus), action.label, PendingIntent.getBroadcast(context, 6980, actionIntent, 0)).build()
    }

    private val tapIntent: PendingIntent
        get() {
            val tapItent = Intent(context, NotificationReceiver::class.java)
            tapItent.action = ActionNames.TAP_ACTION_NAME
            return PendingIntent.getBroadcast(context, 6981, tapItent, 0)
        }

    private val dismissIntent: PendingIntent
        get() {
            val dismissIntent = Intent(context, NotificationReceiver::class.java)
            dismissIntent.action = ActionNames.TAP_ACTION_NAME
            return PendingIntent.getBroadcast(context, 6982, dismissIntent, 0)
        }

}