package ginandi.com.physioball

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.CountDownTimer
import android.util.Log
import ginandi.com.physioball.ActionNames.DISMISS_ACTION_NAME
import ginandi.com.physioball.ActionNames.DONE_ACTION_NAME
import ginandi.com.physioball.ActionNames.SNOOZE_ACTION_NAME
import ginandi.com.physioball.ActionNames.START_ACTION_NAME
import org.joda.time.DateTime


/**
 * Created by gindi on 6/17/17.
 */

class NotificationReceiver : BroadcastReceiver() {

    object ActionMapper {

        val actionMap = hashMapOf<String, (Context) -> Unit>(
                Pair(START_ACTION_NAME, fun(c) {
                    Log.d(this.javaClass.name, "Start")

                    getScheduleManager(c).lastTimeUsePhysioBall = DateTime.now()

                    val serviceIntent = Intent(c, CountdownService::class.java)
                    c.startService(serviceIntent)

                    Unit
                }),
                Pair(DONE_ACTION_NAME, fun(c) {
                    PhysioNotificationManager(c).cancel()
                    stopService(c)
                    LoopingMediaPlayer.stopPlaying()
                    Unit
                }),
                Pair(DISMISS_ACTION_NAME, fun(c) {
                    getScheduleManager(c).lastTimeUsePhysioBall = DateTime.now()
                    PhysioNotificationManager(c).cancel()
                    stopService(c)
                    Unit
                }),
                Pair(SNOOZE_ACTION_NAME, fun(c) {
                    PhysioNotificationManager(c).cancel()
                    stopService(c)
                    Unit
                })
        )

        fun stopService(c: Context) {
            val intent = Intent(c, CountdownService::class.java)
            intent.action = STOP_COMMAND
            c.startService(intent)
        }

        fun takeAction(action: String, context: Context?) = actionMap.getOrDefault(action, {})(context!!)
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.d(this.javaClass.name, "on receive")
        ActionMapper.takeAction(intent?.action ?: "", context)
    }
}